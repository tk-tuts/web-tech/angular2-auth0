/**
 * Created by Keerthikan on 21-Feb-17.
 */
import { Component } from '@angular/core';
import { Auth } from '../../auth.service';

@Component({
  moduleId: module.id,
  selector: 'home',
  templateUrl: './home.component.html',

  providers: [ Auth ]
})
export class HomeComponent {
  constructor(private auth: Auth){

  }
}
